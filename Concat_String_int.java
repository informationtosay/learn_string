package learn_String;

public class Concat_String_int {

	public static void main(String[] args) {
		// 1st INTEGER concat String
		int cost = 100;
		String price = "100";
		String rate = cost + price;
		System.out.println("Rate : " + rate);

		// 2nd String concat String
		String value1 = "1000";
		String value2 = "2000";
		String totalValue = value1.concat(value2);
		System.out.println("Total Value : " + totalValue);

		// 3rd int concat int
		int number1 = 500;
		int number2 = 800;
		int total = number1 + number2;
		System.out.println("Total : " + total);
	}

}
