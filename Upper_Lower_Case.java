package learn_String;

public class Upper_Lower_Case {

	public static void main(String[] args) {

		Upper_Lower_Case ul = new Upper_Lower_Case();
		ul.upper();
		ul.lower();

	}

	public void lower() {

		String name = "RAMU CITTRASAN";
		String name1 = name.toLowerCase();
		System.out.println("TO CHANGE LOWER CASE : " + name1);

	}

	public void upper() {

		String name = "ramu cittrasan";
		String name1 = name.toUpperCase();
		System.out.println("TO CHANGE UPPER CASE : " + name1);
	}

}
