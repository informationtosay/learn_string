package learn_String;

public class Concat_Method {

	public static void main(String[] args) {

		String firstName = "Ramu ";
		String lastName = "C";
		String name = firstName + lastName;
		System.out.println("Name : " + name);
		System.out.println(firstName + lastName);
		String name2 = firstName.concat(lastName);
		System.out.println(name2);
	}

}
