package learn_String;

public class Index_Of {

	public static void main(String[] args) {
		// 1st method int indexOf(char ch )

		String content = "Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can “just run”.";
		int index = content.indexOf("easy"); // IndexOf
		char ch = content.charAt(5);
		System.out.println("index of easy : "+index);
		System.out.println("get indexLetter of content : "+ch);
		String route = new String("Madurai to chennai");
		int noOfPosition = route.indexOf('i'); // Found 'i' first at position
		System.out.println("Found 'i' first at position : " + noOfPosition);

		// 2nd method int indexOf(char ch, int strt)
		String status = new String("I am Happiest man in the world");// Found 'i' after 11th index at position
		int noOf2ndPosition = status.indexOf('i', 11);
		System.out.println("Found 'i' after 11th index at position : " + noOf2ndPosition); // 2nd index of 'i' will print
		
		// 3rd method int indexOf(String str)
		String line = "This method returns the index within this string of the first occurrence of the specified substring. If it does not occur as a substring, -1 is returned.";
		int noOfPositionString = line.indexOf("first");
        System.out.println("noOfPositionString of : "+ noOfPositionString);
		String get = new String("today online class");
        String subget = new String("class");
		int subGetIndex = get.indexOf(subget);
		System.out.println("subGetIndex of : "+subGetIndex);
        
        //4th method int indexOf(String str, int strt)
        String news = "Fourteenth date june 2024 is today date.";
        String update = "date";
        int result = news.indexOf(update,9);
        System.out.println("result of news : "+result);
	}

}
