package learn_String;

public class String_Special_Charactors {

	public static void main(String[] args) {
		// 1st --> Double quote escape character, The sequence \" inserts a double quote
		// in a string:
		String text = "Prime Minister Narendra Modi re-shared the video and wrote, \"Long live India-Italy friendship!\".";
		System.out.println(text);

		// 2nd --> Backslash escape character, The sequence \\ inserts a single
		// backslash in a string:
		String line1 = "today \\ tommorow finish the work."; // \ --> inserts a single backslash in a string
		String line2 = "today \n tommorow finish the work."; // new line create \n
		String line3 = "today \r tommorow finish the work."; // Carriage Return \r
		String line4 = "today\ttommorow finish the work."; // Tab \t

		System.out.println(line1);
		System.out.println(line2);
		System.out.println(line3);
		System.out.println(line4);

	}

}
